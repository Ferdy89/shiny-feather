# Shiny Feather

Twitter bot that retweets anything that shows up in the timeline of the
associated account. Built in Ruby, meant to be run as a cron job on a Raspberry
Pi or similar.

## Building

```
bundle install
```

## Configuration

Shiny Feather relies on these 4 environment variables:

```
SHINY_FEATHER_CONSUMER_KEY        # API key
SHINY_FEATHER_CONSUMER_SECRET     # API secret key
SHINY_FEATHER_ACCESS_TOKEN        # Access token
SHINY_FEATHER_ACCESS_TOKEN_SECRET # Access token secret
```

You can get these from the registered app at [Twitter
Developer](https://developer.twitter.com/en/apps).

## Automation

You can run Shiny Feather recurrently with cron and chruby. The recommended
approach I'd suggest is creating a quick script in
`/path/to/somewhere/shiny-feather.sh`:

```
#!/bin/bash

source /usr/local/share/chruby/chruby.sh

export SHINY_FEATHER_CONSUMER_KEY=something
export SHINY_FEATHER_CONSUMER_SECRET=something
export SHINY_FEATHER_ACCESS_TOKEN=something
export SHINY_FEATHER_ACCESS_TOKEN_SECRET=something

chruby ruby-3.1.2

/path/to/the/repo/bin/bundle exec ruby /path/to/the/repo/shiny_feather.rb
```

Then run `chmod +x /path/to/somewhere/shiny-feather.sh`;

Finally, install on your crontab running `crontab -e` and adding a line like:

```
# Check out https://github.com/postmodern/chruby/wiki/Cron for more info
SHELL=/bin/bash
HOME=/home/you
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin

* * * * * /path/to/somewhere/shiny-feather.sh
```

## Use case

Shiny Feather is currently being used with the @FIUPM Twitter account.
