# frozen_string_literal: true

require "twitter"

client = Twitter::REST::Client.new do |config|
  config.consumer_key        = ENV["SHINY_FEATHER_CONSUMER_KEY"]
  config.consumer_secret     = ENV["SHINY_FEATHER_CONSUMER_SECRET"]
  config.access_token        = ENV["SHINY_FEATHER_ACCESS_TOKEN"]
  config.access_token_secret = ENV["SHINY_FEATHER_ACCESS_TOKEN_SECRET"]
end

client.home_timeline.reject(&:retweeted?).each do |tweet|
  client.retweet(tweet)
  puts "Retweeted #{tweet}"
end
client.mentions_timeline.reject(&:retweeted?).each do |tweet|
  client.retweet(tweet)
  puts "Retweeted #{tweet}"
end
